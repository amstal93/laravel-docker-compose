# Laravel Docker Compose Setup

Copy the following files into your new laravel project folder:

- `.dockerignore`
- `Dockerfile`
- `docker-compose.yml`

## Requirements

- Docker version 19.03.3, build a872fc2f86 or higher
- docker-compose version 1.25.0, build 0a186604 or higher

## Setup

Copy the environment file with:
```
cp .env.example .env
```
... and fill in the values.

Install composer packages to your hard drive. From a terminal within the project root, execute:
```
docker run --rm --interactive --tty --volume $PWD:/app  --user $(id -u):$(id -g) composer:1.9.3 install
```

Install npm packages to your hard drive with:
```
docker-compose run --rm --no-deps app npm install
```

Fix the permissions on the installed packages with:
```
docker-compose run --rm --no-deps app chown -R www-data:www-data vendor node_modules
```

Now generate an app key:
```
docker-compose run --rm --no-deps app php artisan key:generate
```

## Usage

### Start

Start the environment with:
```
docker-compose up -d
```

Visit the website under `http://localhost`.

### Regular tasks

You will probably want to run some commands from inside the container. You can do anything you would normally do by executing a command like this:
```
docker-compose run --rm --user="www-data" app ${YOUR_COMMAND}
```

For example:
```
docker-compose run --rm --user="www-data" app php artisan migrate:fresh --seed
```

Omit `--user="www-data"` in order to act as root within the container.

### Stop

Shut down the environment with:
```
docker-compose down
```

## Note

The Dockerfile uses an upstream image created by yours truly. 
You can find it on [DockerHub](https://hub.docker.com/repository/docker/malikdirim/laravel). 
There is a [GitHub Repository](https://github.com/leeniu/laravel-docker) that creates these images. 
You are welcome to participate there, too.
